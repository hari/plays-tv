<?php
session_start();

//Handles AJAX call

require_once 'vendor/autoload.php';
require_once 'autoload.php';
require_once 'src/db_config.php';
$_ENV = Utils::loadConfig();

if (isset($_GET['game_id'])) {
  $api = new ApiRequest();
  if (isset($_GET['search_limit'])) {
    $_SESSION['search_limit'] = intval($_GET['search_limit']);
  }
  if (isset($_GET['date_limit'])) {
    $_SESSION['date_limit'] = intval($_GET['date_limit']);
  }
  $response;
  if ($_SESSION['search_limit'] < 0 && $_SESSION['search_limit'] != -1) {
    $response = [];
  } else {
    try {
      $response = $api->getVideos($_GET['game_id'], $_GET['limit'], $_GET['page']);
    } catch (Exception $e) {
      $response = [];
    }
  }
  $valid = 0;
  foreach($response as $video) {
    if ($_SESSION['search_limit'] < 0 && $_SESSION['search_limit'] != -1) {
      break;
    }
    if (Video::save($video)) {
      $valid++;
      $_SESSION['search_limit']--;
    }
  }
  echo json_encode(array(
    'next_page' => $_SESSION['next_page'],
    'limit' => $_SESSION['limit'],
    'game_id' => $_SESSION['game_id'],
    'videos' => $response,
    'valid_count' => $valid,
    'game_count' => VideoTable::getCount('game_id'),
    'video_count' => VideoTable::getCount('video_id')
  ));
}elseif (isset($_GET['delete'])) {
  if (VideoTable::delete()) {
    echo "{ \"data\": " . VideoTable::getCount('video_id') . " }";
  } else {
    echo "{ 'data': 'failed' }";
  }
} else {
  var_dump($_SESSION);
}