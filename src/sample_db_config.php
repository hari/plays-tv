<?php
/**
 * This is a sample `db_config.php` file. The `db_config.php` can be generated automatically by the script.
 * If you want you can simply fill up these fields and rename the file to `db_config.php`,
 * there won't be need for generating the file by script
 */

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'playtv'); // the database name to use
define('SETTING_TABLE_NAME', 'setting'); // the setting table, it will be created, so must be unique
define('VIDEO_TABLE_NAME', 'videos'); // the videos table, it will be created, so must be unique