<?php
if (!(isset($_SESSION) && isset($_SESSION['user']))) {
    die('Authorization failed.');
}
$game_count = VideoTable::getCount('game_id');
$video_count = VideoTable::getCount('video_id');

$game_id = (isset($_SESSION['game_id'])) ? $_SESSION['game_id'] : '';
$page = (isset($_SESSION['next_page'])) ? $_SESSION['next_page'] : 1;
$search_limit = (isset($_SESSION['search_limit'])) ? $_SESSION['search_limit'] : 5000;
$date_limit = (isset($_SESSION['date_limit'])) ? $_SESSION['date_limit'] : 30;
--$page;
$limit = (isset($_SESSION['limit'])) ? $_SESSION['limit'] : 400;
?>

<div class="row">
<section class="columns six">
  <h4 class="blue">Make request</h4>
  <form onsubmit="callAPI(this);return false;">
   <div class="row">
    <div class="columns four">
      <label>GAME ID</label>
      <input type="text" name="game_id" value="<?php echo $game_id; ?>" />
    </div>
    <div class="columns four">
      <label>STARTING PAGE</label>
      <input type="number" name="page" value="<?php echo $page; ?>" />
    </div>
    <div class="columns four">
      <label>LIMIT</label>
      <input type="number" name="limit" value="<?php echo $limit; ?>" />
    </div>
   </div>
   <div class="row">
    <div class="columns four">
      <label>MAX RESULT</label>
      <input type="number" name="search_limit" value="<?php echo $search_limit; ?>" />
    </div>
    <div class="columns four">
      <label>WITHIN DAYS</label>
      <input type="number" name="date_limit" value="<?php echo $date_limit; ?>" />
    </div>
    <div class="columns four">
      <label> &nbsp; </label>
      <button type="submit">FETCH</button>
    </div>
   </div>
  </form>
  <div class="row info">
    Total: <strong id="total">0</strong>
  </div>
  <ol class="box" id="videos">
    <li></li>
  </ol>
</section>
<section class="columns six">
  <h4 class="green">Other Information</h4>
  <ol class="box">
    <li class="box-row">
      <h5>App</h5>
      <div class="row">
        <p>Status: <strong style="color: #58f" id="status">Ready</strong></p>
        <p>ID:<strong> <?php echo $_ENV['APP_ID']; ?></strong></p>
        <p>Key:<strong> <?php echo $_ENV['APP_KEY']; ?></strong></p>
      </div>
    </li>
    <li class="box-row">
      <h5>Admin</h5>
      <div class="row">
        <p>Name:<strong> <?php echo $_ENV['NAME']; ?></strong></p>
        <p>Email:<strong> <?php echo $_ENV['EMAIL']; ?></strong></p>
      </div>
    </li>
    <li class="box-row">
      <h5>Database</h5>
      <div class="row">
        <p>Unique Games:<strong id="game_count"> <?php echo $game_count; ?></strong></p>
        <p>Total Videos:<strong id="video_count"> <?php echo $video_count; ?></strong></p>
        <a class="button-small u-pull-right" onclick="deleteVideos();" href="javascript://">Delete</a>
      </div>
    </li>
  </ol>
</section>
</div>
<script type="text/javascript">
    (function (a) { function b() { this._callbacks = []; } b.prototype.then = function (a, c) { var d; if (this._isdone) d = a.apply(c, this.result); else { d = new b(); this._callbacks.push(function () { var b = a.apply(c, arguments); if (b && typeof b.then === 'function') b.then(d.done, d); }); } return d; }; b.prototype.done = function () { this.result = arguments; this._isdone = true; for (var a = 0; a < this._callbacks.length; a++)this._callbacks[a].apply(null, arguments); this._callbacks = []; }; function c(a) { var c = new b(); var d = []; if (!a || !a.length) { c.done(d); return c; } var e = 0; var f = a.length; function g(a) { return function () { e += 1; d[a] = Array.prototype.slice.call(arguments); if (e === f) c.done(d); }; } for (var h = 0; h < f; h++)a[h].then(g(h)); return c; } function d(a, c) { var e = new b(); if (a.length === 0) e.done.apply(e, c); else a[0].apply(null, c).then(function () { a.splice(0, 1); d(a, arguments).then(function () { e.done.apply(e, arguments); }); }); return e; } function e(a) { var b = ""; if (typeof a === "string") b = a; else { var c = encodeURIComponent; var d = []; for (var e in a) if (a.hasOwnProperty(e)) d.push(c(e) + '=' + c(a[e])); b = d.join('&'); } return b; } function f() { var a; if (window.XMLHttpRequest) a = new XMLHttpRequest(); else if (window.ActiveXObject) try { a = new ActiveXObject("Msxml2.XMLHTTP"); } catch (b) { a = new ActiveXObject("Microsoft.XMLHTTP"); } return a; } function g(a, c, d, g) { var h = new b(); var j, k; d = d || {}; g = g || {}; try { j = f(); } catch (l) { h.done(i.ENOXHR, ""); return h; } k = e(d); if (a === 'GET' && k) { c += '?' + k; k = null; } j.open(a, c); var m = 'application/x-www-form-urlencoded'; for (var n in g) if (g.hasOwnProperty(n)) if (n.toLowerCase() === 'content-type') m = g[n]; else j.setRequestHeader(n, g[n]); j.setRequestHeader('Content-type', m); function o() { j.abort(); h.done(i.ETIMEOUT, "", j); } var p = i.ajaxTimeout; if (p) var q = setTimeout(o, p); j.onreadystatechange = function () { if (p) clearTimeout(q); if (j.readyState === 4) { var a = (!j.status || (j.status < 200 || j.status >= 300) && j.status !== 304); h.done(a, j.responseText, j); } }; j.send(k); return h; } function h(a) { return function (b, c, d) { return g(a, b, c, d); }; } var i = { Promise: b, join: c, chain: d, ajax: g, get: h('GET'), post: h('POST'), put: h('PUT'), del: h('DELETE'), ENOXHR: 1, ETIMEOUT: 2, ajaxTimeout: 0 }; if (typeof define === 'function' && define.amd) define(function () { return i; }); else a.promise = i; })(this);
</script>
<script type="text/javascript">

var eleSts = document.getElementById('status');
function setStatus(sts) {
  eleSts.textContent = sts;
}

function deleteVideos() {
  makeRequest('ajax.php?delete=all', deleted);
}

function deleted(response) {
  if (response) {
    document.getElementById('video_count').textContent = response.data;
  }
}

 function callAPI(form) {
   if (form.game_id.value.trim().length == 0) {
     alert('Please enter game id!');
   } else {
     setStatus('Making request');
     showLoading(document.getElementById('videos'));
     var query = 'game_id='+form.game_id.value+'&page='
                 +form.page.value+'&limit='+form.limit.value
                 +'&search_limit='+form.search_limit.value+'&date_limit='+form.date_limit.value;;
     makeRequest('ajax.php?'+query, render);
   }
 }

 function showLoading(node) {
   clearNode(node);
   var li = document.createElement('li');
   li.innerHTML = '<img src="public/images/wave-loader.gif" alt="Loading" />';
   node.appendChild(li);
 }

 function render(data) {
   setStatus('Rendering');
   var html = `<h5>%name%</h5>
      <div class="row">
        <p class="u-pull-left">VID: <strong> %vid%</strong></p>
        <p class="u-pull-right">RID:<strong> %rid%</strong></p>
      </div>
      <div class="row">
        <p class="u-pull-left">Author: <strong> %author%</strong></p>
        <p class="u-pull-right">Link:<strong> %link%</strong></p>
      </div>`;
   var target = document.getElementById('videos');
   document.getElementById('total').textContent = data.videos.length;
   document.getElementById('game_count').textContent = data.game_count
   document.getElementById('video_count').textContent = data.video_count;
   var form = document.forms[0];
   form.limit.value = data.limit;
   form.game_id.value = data.game_id;
   form.page.value = data.next_page;
   form.search_limit.value = parseInt(form.search_limit.value) - data.valid_count;
   if (form.search_limit.value < 0 && form.search_limit.value != -1) {
     form.search_limit.value = 0;
   }
   clearNode(target);
   for(var o in data.videos) {
     var li = document.createElement('li');
     var video = data.videos[o];
     li.className = 'row box-row';
     li.innerHTML = html.replace('%name%', video.description)
                        .replace('%rid%', video.id)
                        .replace('%vid%', video.video_id)
                        .replace('%author%', video.author.id)
                        .replace('%link%', video.link);
     target.appendChild(li);
   }
   if (data.videos.length == 0 || form.search_limit.value == 0) {
     return setStatus('Done');
   }
   setStatus('Preparing next call!');
   setTimeout(function(){
     callAPI(form);
   }, 5000);
 }

 function makeRequest(url, callback) {
     promise
     .get(url)
     .then(function (error, response, xhr) {
       if (error) {
         return alert('Opps! error occurred.');
       }
       try {
         console.log(response);
         response = JSON.parse(response);
         callback(response);
       } catch (e) {
         alert(e.message);
       }
     });
 }

 function clearNode(node) {
   while (node.firstChild) {
     node.removeChild(node.firstChild);
   }
 }

</script>
