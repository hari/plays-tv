<?php
// main view file
?>
<!doctype html>
<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width,inital-scale=1.0">
 <link href="public/app.css" rel="stylesheet" />
 <title>The Video</title>
</head>
<body>
 <div class="container">
 <?php include 'src/views/'.$current_view; ?>
 </div>
</body>
</html>
