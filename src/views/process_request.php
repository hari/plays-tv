<?php

//Saves the db_config or other configuration from POST request

$empty_fields = Utils::findEmptyKey($_POST);
if (empty($empty_fields)) {
    if (isset($_POST['DB_HOST'])) {
        Utils::createConfig();
        try {
            require_once 'src/db_config.php';
            if (SettingTable::create()) {
                VideoTable::create();
                echo '<h3 class="green">Success</h3>';
                echo '<p style="margin-bottom:8px">The database settings have been saved.</p>';
                echo '<a href="/">Home</a>';
            } else {
                error();
            }
        } catch (Exception $e) {
            error();
        }
    } else {
        Utils::saveSetting();
        echo '<h3 class="green">Success</h3>';
        echo '<p style="margin-bottom:8px">The settings have been saved.</p>';
        echo '<a href="/">Home</a>';
    }
} else {
    echo '<h3 class="red">Error</h3>';
    echo '<p style="margin-bottom:8px">Opps! you forgot to fill <strong>'.implode(',', $empty_fields).'</strong></p>';
    echo '<a href="/">Try Again</a>';
}

function error()
{
    unlink('src/db_config');
    echo '<h3 class="red">Error</h3>';
    echo '<p>The database settings didn\'t work.</p>';
    echo '<ol><li>Please make sure username and password is correct.</li><li>Please make sure database has been created.</li></ol>';
    echo '<a href="/">Try Again</a>';
}
