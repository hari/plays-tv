<?php

/**
 * A base class for both API to interface the requests.
 */
class ApiRequest
{
    private $_api_url = "https://api.plays.tv/data/v1/videos/search";
    //?appid=NIBoOI-oNYO_BLGaNYSaC2VkCLJtNqKYO2Jq
    //&appkey=dN7usIrA0QeBjfzS1AJCLH4blC46eHJ4
    //&gameId=beab9bd40164e6ed6101d7e0914b59e1
    //&limit=1500&page=0&sort=recent&sortdir=desc;
    private $_headers = array();

    public function __construct()
    {

    }
  
    public function addHeader($key, $value)
    {
        $this->_headers[$key] = $value;
    }

    public function removeHeader($key)
    {
        if (!array_key_exists($key, $this->_headers)) {
            return false;
        }
        unset($this->_headers[$key]);
    }

    public function getHeaders()
    {
        return $this->_headers;
    }

    protected function buildQuery($params = array())
    {
        $query = '&';
        if (count($params) > 0) {
            $query = '?';
            foreach ($params as $key => $value) {
                $query .= "$key=$value&";
            }
        }
        //remove last &
        return substr($query, 0, strlen($query) - 1);
    }

    protected function getUrl($game_id, $limit, $page)
    {
        global $_ENV;
        $params = array(
            'appid' => $_ENV['APP_ID'],
            'appkey' => $_ENV['APP_KEY'],
            'gameId' => $game_id,
            'limit' => $limit,
            'page' => $page,
            'sort' => 'recent',
            'sortdir' => 'desc'
        );
        return $this->_api_url.$this->buildQuery($params);
    }

    public function getVideos($game_id, $limit, $page = 0)
    {
        $_SESSION['limit'] = $limit;
        $_SESSION['game_id'] = $game_id;
        $url = $this->getUrl($game_id, $limit, $page);
        $response = Requests::get($url);
        if ($response->status_code != '200') {
            return array();
        }
        $response = json_decode($response->body)->content;
        if (!isset($_SESSION['max']) || ($_SESSION['max'] > 0 && $_SESSION['max'] != $response->total_results)) {
            // store for first time.
            $_SESSION['max'] = intval($response->total_results);
        } else {
            // subtract current page result
            $_SESSION['max'] -= intval($response->result_count);
            // done!
            if ($_SESSION['max'] <= 0) {
                // unset it for future calls
                unset($_SESSION['max']);
            }
        }
        $_SESSION['next_page'] = intval($response->result_page) + 1;
        return $response->items;
    }

    public function nextPage() {
        if (!isset($_SESSION['max']) || $_SESSION['max'] <= 0) {
            return array();
        }
        return $this->getVideos($_SESSION['game_id'], $_SESSION['limit'], $_SESSION['next_page']);
    }
    
    
}
