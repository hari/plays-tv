<?php

class VideoTable
{

    public static function create()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `".VIDEO_TABLE_NAME."` ( `row_id` VARCHAR(200) NOT NULL , `video_id` VARCHAR(300) NOT NULL , `video_url` TEXT NOT NULL , `thumb_url` TEXT NOT NULL , `video_title` TEXT NOT NULL , `user` VARCHAR(50) NOT NULL , `game_id` VARCHAR(300) NOT NULL , `uploaded_time` DATE NOT NULL, UNIQUE (`video_id`), PRIMARY KEY (`row_id`));";
        return Connection::execute($sql);
    }

    public static function insert($video)
    {
        if ($_SESSION['date_limit'] != -1 && floor((time() - strtotime($video->uploaded_at))/(60 * 60 * 24)) > $_SESSION['date_limit']) {
            $_SESSION['search_limit'] = -10; //change limit to stop execution
            return false;
        }
        $sql = sprintf('INSERT INTO `%s` (`row_id`, `video_id`, `video_url`, `thumb_url`,`video_title`, `user`, `game_id`, `uploaded_time`) 
               VALUES (:rid, :vid, :url, :turl, :title, :user, :gid, :ut)', VIDEO_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':rid', $video->row_id, PDO::PARAM_STR);
        $stm->bindParam(':vid', $video->video_id, PDO::PARAM_STR);
        $stm->bindParam(':url', $video->url, PDO::PARAM_STR);
        $stm->bindParam(':turl', $video->thumb_url, PDO::PARAM_STR);
        $stm->bindParam(':title', $video->title, PDO::PARAM_STR);
        $stm->bindParam(':user', $video->author_id, PDO::PARAM_STR);
        $stm->bindParam(':gid', $video->game_id, PDO::PARAM_STR);
        $stm->bindParam(':ut', $video->uploaded_at, PDO::PARAM_STR);
        return $stm->execute();
    }

    public static function getById($id)
    {
        $sql = sprintf('SELECT * FROM `%s` WHERE `row_id` = :id', VIDEO_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':id', $name, PDO::PARAM_STR);
        if ($stm->execute()) {
            return $stm->fetch();
        }
        return null;
    }

    public static function getCount($column) {
        $sql = sprintf('SELECT COUNT(DISTINCT %s) FROM %s', $column, VIDEO_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        if ($stm->execute()) {
            $result = $stm->fetch();
            if (count($result) > 0) {
                return $result[array_keys($result)[0]];
            }
        }
        return 0;
    }

    public static function delete() {
        $sql = 'DELETE FROM '. VIDEO_TABLE_NAME;
        return Connection::get()->prepare($sql)->execute();
    }
}
