<?php

class SettingTable
{

    public static function create()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `".SETTING_TABLE_NAME."` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL COMMENT 'name of the setting' , `value` TEXT NOT NULL COMMENT 'value for this setting' , `created_at` DATE NOT NULL COMMENT 'When was this setting first created?' , `updated_at` DATE NOT NULL COMMENT 'When was this setting updated?' , PRIMARY KEY (`id`));";
        return Connection::execute($sql);
    }

    public static function put($name, $value)
    {
        $existing = self::get($name);
        if ($existing == null) {
            //new setting, need to store it
            return self::insert($name, $value);
        } elseif ($existing['value'] == $value) {
            //no need to run query
            return true;
        }
        //old setting need to update
        return self::update($name, $value);
    }

    private static function update($name, $value)
    {
        $sql = sprintf('UPDATE `%s` SET `value`= :value, `updated_at` = :updated_date WHERE `name` = :name', SETTING_TABLE_NAME);
        date_default_timezone_set("Asia/Bangkok");
        $now = date('Y-m-d');
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':name', $name, PDO::PARAM_STR);
        $stm->bindParam(':value', $value, PDO::PARAM_STR);
        $stm->bindParam(':updated_date', $now, PDO::PARAM_STR);
        return $stm->execute();
    }

    private static function insert($name, $value)
    {
        $sql = sprintf('INSERT INTO `%s` (`name`,`value`, `created_at`, `updated_at`) VALUES (:name, :value, :c, :u)', SETTING_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':name', $name, PDO::PARAM_STR);
        $stm->bindParam(':value', $value, PDO::PARAM_STR);
        date_default_timezone_set("Asia/Bangkok");
        $now = date('Y-m-d');
        $stm->bindParam(':c', $now, PDO::PARAM_STR);
        $stm->bindParam(':u', $now, PDO::PARAM_STR);
        return $stm->execute();
    }

    public static function get($name)
    {
        $sql = sprintf('SELECT * FROM `%s` WHERE `name` = :name', SETTING_TABLE_NAME);
        $stm = Connection::get()->prepare($sql);
        $stm->bindParam(':name', $name, PDO::PARAM_STR);
        if ($stm->execute()) {
            return $stm->fetch();
        }
        return null;
    }
}
