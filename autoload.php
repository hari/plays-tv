<?php

/**
 * Autoload custom classes
 */

$folders = [
  'src/api/',
  'src/database/',
  'src/models/'
];

spl_autoload_register(function ($name) {
  global $folders;
  foreach($folders as $folder) {
    if (file_exists($folder."$name.php")) {
      return require_once $folder."$name.php";
    }
  }
});